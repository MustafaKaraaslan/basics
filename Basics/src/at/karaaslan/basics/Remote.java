package at.karaaslan.basics;

import java.util.ArrayList;
import java.util.List;

public class Remote {

	private int lenght;
	private int width;
	private int weight;
	private String color;
	private int serialNumber;
	private String remote;
	private List<Device> devices;
	
	
	public Remote(int lenght, int width, int weight, String color, int serialNumber, String remote) {
		super();
		this.lenght = lenght;
		this.width = width;
		this.weight = weight;
		this.color = color;
		this.serialNumber = serialNumber;
		this.remote = remote;
		this.devices = new ArrayList<>();
	}

	public void addDevice(Device dev) {
		this.devices.add(dev);
	}
	
	public void turnOn() {
		System.out.println("I am turned ON now-" + this.remote);
	}
	
	public void turnOff() {
		System.out.println("I am turned OFF now-" + this.remote);
	}
	
	
	public void getInformation() {
		System.out.println("I have the color " + this.color + " and my Serial Number is " + this.serialNumber + ".");
	}


	public void setColor(String color) {
		this.color = color;
	}


	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}
	
	
	
	
	
	
	
}
