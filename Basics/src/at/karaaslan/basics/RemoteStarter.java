package at.karaaslan.basics;

import at.karaaslan.basics.Device.type;

public class RemoteStarter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Remote r1 = new Remote(100,30,300,"green",1001,"r1");
		Remote r2 = new Remote(200,50,250,"blue",1002,"r2");
		
		Device d1 = new Device("682531",Device.type.rollo);
		Device d2 = new Device("526487",Device.type.beamer);
		Device d3 = new Device("843576",Device.type.television);
		
		r1.addDevice(d1);
		r1.addDevice(d2);
		r1.addDevice(d3);
		
		for (Device device : r1.getDevices()) {
			System.out.println(device.getSerialNumber()+ " " + device.getDeviceType());
		}
		
		/*
		r1.turnOn();
		r2.turnOn();
		
		r2.setSerialNumber(1009);
		r1.setColor("yellow");
		
		r1.getInformation();
		r2.getInformation();
		
		r1.turnOff();
		r2.turnOff();
		*/
	}

}
