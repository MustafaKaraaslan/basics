package at.karaaslan.basics;

public class Device {
	private String serialNumber;
	private type deviceType;
	
	public enum type {
		television, rollo, beamer
	}
	
	public Device(String serialNumber, type deviceType) {
		super();
		this.serialNumber = serialNumber;
		this.deviceType = deviceType;
	
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public type getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(type deviceType) {
		this.deviceType = deviceType;
	}
	
	
	
	
}
