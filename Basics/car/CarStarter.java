package at.karaaslan.car;

import java.time.LocalDate;


public class CarStarter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Manufacturer m1 = new Manufacturer("Audi","Berlin",4);
		Manufacturer m2 = new Manufacturer("Mercedes","Stuttgart",2);
		Engine e1 = new Engine("Benzin","130ps");
		Engine e2 = new Engine("Diesel","180ps");
		Car c1 = new Car(270,120000,5,"white", e1, m1);
		Car c2 = new Car(240,300000,7,"yellow", e2, m2);
		Person p1 = new Person("Mustafa", "Karaaslan", LocalDate.of(2000, 9, 25));
		
		p1.addCar(c1);
		p1.addCar(c2);
		
		//System.out.println(p1.getfName());
		
		//System.out.println(c1.getPrice());
		
		//System.out.println(e1.getEngine());
	
		System.out.println(p1.valueCars() + "�");
	}

}
