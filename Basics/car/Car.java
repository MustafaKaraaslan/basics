package at.karaaslan.car;

import java.util.ArrayList;
import java.util.List;


public class Car {
	private int maxSpeed;
	private int basePrice;
	private int baseConsumtion;
	private String color;
	private Engine motor;
	private Manufacturer manufaturer;
	
	
	
	public Car(int maxSpeed, int basePrice, int baseConsumtion, String color, Engine motor, Manufacturer manufacturer) {
		super();
		this.maxSpeed = maxSpeed;
		this.basePrice = basePrice;
		this.baseConsumtion = baseConsumtion;
		this.color = color;
		this.motor = motor;
		this.manufaturer = manufacturer;
		
	}
 

	
	
	public int getPrice() {
		int fprice = basePrice - (basePrice / 100 * this.manufaturer.getDiscount()); 
		return fprice;
		
	}
	

	public int getMaxSpeed() {
		return maxSpeed;
	}


	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}


	public int getBasePrice() {
		return basePrice;
	}


	public void setBasePrice(int basePrice) {
		this.basePrice = basePrice;
	}


	public int getBaseConsumtion() {
		return baseConsumtion;
	}


	public void setBaseConsumtion(int baseConsumtion) {
		this.baseConsumtion = baseConsumtion;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public Engine getMotor() {
		return motor;
	}


	public void setMotor(Engine motor) {
		this.motor = motor;
	}
	
	

}


